package com.indra.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indra.crud.model.EmployeeEntity;

@Repository
public interface EmployeesRepository extends JpaRepository<EmployeeEntity, Integer> {

}
