package com.indra.crud.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indra.crud.model.JobHistoryEntity;
import com.indra.crud.model.PKJobHistory;

@Repository //PonerAnotaciones!!
public interface JobHistoryRepository extends JpaRepository<JobHistoryEntity, PKJobHistory> {
	
	

}
