package com.indra.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indra.crud.model.JobEntity;

@Repository
public interface JobsRepository extends JpaRepository<JobEntity, String> {

}
