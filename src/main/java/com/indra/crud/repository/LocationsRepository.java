package com.indra.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indra.crud.model.LocationEntity;


@Repository
public interface LocationsRepository extends JpaRepository<LocationEntity, Integer> {
	
	

}
