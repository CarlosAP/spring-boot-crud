package com.indra.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indra.crud.model.EntityRegion;

@Repository
public interface RegionsRepository extends JpaRepository<EntityRegion, Integer>{
	
	

}
