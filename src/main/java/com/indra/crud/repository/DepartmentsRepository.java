package com.indra.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indra.crud.model.DepartmentEntity;

@Repository
public interface DepartmentsRepository extends  JpaRepository<DepartmentEntity, Integer>{
	

	
	
}
