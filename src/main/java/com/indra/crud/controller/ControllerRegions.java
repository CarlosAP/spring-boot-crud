package com.indra.crud.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.indra.crud.model.EntityRegion;
import com.indra.crud.services.RegionsServices;


@RestController
public class ControllerRegions {
	
	@Autowired
	private RegionsServices regionsServices;
	
	
	@GetMapping("/regions")///Listo
	public List<EntityRegion> getRegions(){
		return regionsServices.getRegions();
	}
	
	
	@GetMapping("/regions/{regionId}")//Listo
	public ResponseEntity<Object> getRegionById(@PathVariable int regionId){
		return regionsServices.getRegionById(regionId);
	}
	
	@PostMapping("/regions/create")//Listo(Se debe asignar el id)
	public ResponseEntity<Object> createRegion(@RequestBody EntityRegion region){ //requestbody* (recibe un json por esa razon usamos la notacion)
		ResponseEntity<Object> savedRegion = regionsServices.createRegion(region);
		return null;
	}
	
	@PutMapping("/regions/update/{regionId}")
	public void updateRegion(@PathVariable int regionId, @RequestBody EntityRegion region) {
		regionsServices.updateRegion(regionId, region);
	}
	
	@DeleteMapping("/regions/delete/{regionId}")//Listo(error......)
	public void deleteRegion(@PathVariable int regionId) {
		regionsServices.deleteRegion(regionId);
	}
	
	
}
