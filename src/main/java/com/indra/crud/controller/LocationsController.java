package com.indra.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.indra.crud.model.LocationEntity;
import com.indra.crud.services.LocationsServices;

@RestController
public class LocationsController {

	
	@Autowired
	private LocationsServices services;
	
	@GetMapping("/locations")///Listo
	public List<LocationEntity> getLocations(){
		return services.listar();
	}
	
	
	@GetMapping("/locations/{locationId}")//Listo ---el nombre del parametro dentro debe ser igual al parametro que recibe el metodo
	public ResponseEntity<Object> getJobById(@PathVariable int locationId){
		return services.listarPorId(locationId);
	}
	
	@PostMapping("/locations/create")//Listo(Se debe asignar el id)//
	public ResponseEntity<Object> createLocation(@RequestBody LocationEntity location){ //requestbody* (recibe un json por esa razon usamos la notacion)
		ResponseEntity<Object> savedLocation = services.create(location);
		return null;
	}
	
	@PutMapping("/locations/update/{locationId}")//Listo*********
	public void updateLocation(@PathVariable int locationId, @RequestBody LocationEntity location) {
		services.update(locationId, location);
	}
	
	@DeleteMapping("/locations/delete/{locationId}")//Listo
	public void deleteLocation(@PathVariable int locationId) {
		services.delete(locationId);
	}
	
	
	
	
}
