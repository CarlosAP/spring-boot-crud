package com.indra.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.indra.crud.model.CountriyEntity;
import com.indra.crud.model.JobHistoryEntity;
import com.indra.crud.model.PKJobHistory;
import com.indra.crud.services.CountriesServices;
import com.indra.crud.services.JobHistoryServices;

@RestController
public class JobHistoryController {

	

	@Autowired
	private JobHistoryServices jobService;
	
	
	@GetMapping("/jobhistory")///Listo
	public List<JobHistoryEntity> getJobsHistory(){
		return jobService.listar();
	}
	
	
	@GetMapping("/jobhistory/id")//Listo(Revisar formato en el que debe enviarse la fecha )
	public ResponseEntity<Object> getJobHistoryById(@RequestBody PKJobHistory id){ //Recibe un json que contiene los dos id(emoloyee id and startDate)
		return jobService.listarPorId(id);
		/*exaple
		 * {
	        "employeeId": 206,
	        "startDate": "2004-07-20"
			}
		 */	
	}
	
	@PostMapping("/jobhistory/create")//Listo(revisar las restricciones que contiene la tabla )
	public ResponseEntity<Object> createJobHistory(@RequestBody JobHistoryEntity employee){ //requestbody* (recibe un json por esa razon usamos la notacion)
		ResponseEntity<Object> savedJob = jobService.create(employee);
		return null;
		/*
		 example: *start date rango*
		{
	    "miPK": {
	        "employeeId": 206,
	        "startDate": 	"2005-07-20" 
    		},
	    "endDate": "2006-12-31",
	    "jobId":"AC_ACCOUNT",
	    "departmentId": 60
		}
		 */
	}
	
	@PutMapping("/jobhistory/update")
	public void updateJobHistory(@RequestBody PKJobHistory employee, JobHistoryEntity job) {
		jobService.update(employee, job);
	}
	
	@DeleteMapping("/jobhistory/delete")//Listo 
	public void deleteJobHistory(@RequestBody PKJobHistory id) { //Rcibe un json con los dos id que se necesitan para relizaer la busqueda y eliminar
		jobService.delete(id);
		
		/*exaple
		 * {
	        "employeeId": 206,
	        "startDate": "2004-07-20"
			}
		 */		
	}
	
	
	
}
