package com.indra.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.indra.crud.model.CountriyEntity;
import com.indra.crud.model.EmployeeEntity;
import com.indra.crud.services.EmployeesServices;

@RestController
public class EmployeesController {

	
	@Autowired
	private EmployeesServices service;
	
	@GetMapping("/employees")///Listp
	public List<EmployeeEntity> getEmployees(){
		return service.listar();
	}
	
	
	@GetMapping("/employees/{employeeId}")//Listo
	public ResponseEntity<Object> getCountry(@PathVariable int employeeId){
		return service.listarPorId(employeeId);
	}
	
	@PostMapping("/employees/create")//Listo**Checar restricciones 
	public ResponseEntity<Object> createEmployee(@RequestBody EmployeeEntity employee){ //requestbody* (recibe un json por esa razon usamos la notacion)
		ResponseEntity<Object> savedEmployee = service.create(employee);
		return null;
	}
	
	@PutMapping("/employees/update/{employeeId}")
	public void updateEmployee(@PathVariable int employeeId, @RequestBody EmployeeEntity employee) {
		service.update(employeeId, employee);
	}
	
	@DeleteMapping("/employees/delete/{employeeId}")//Listo
	public void deleteEmployee(@PathVariable int employeeId) {
		service.delete(employeeId);
	}
	
	
	
}



