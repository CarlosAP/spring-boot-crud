package com.indra.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.indra.crud.model.JobEntity;
import com.indra.crud.services.JobsServices;


@RestController
public class ControllerJobs {
	
	
	@Autowired
	private JobsServices jobsServices;
	
	
	@GetMapping("/jobs")///Listo
	public List<JobEntity> getJobs(){
		return jobsServices.listar();
	}
	
	
	@GetMapping("/jobs/{jobId}")//Listo ---el nombre del parametro dentro debe ser igual al parametro que recibe el metodo
	public ResponseEntity<Object> getJobById(@PathVariable String jobId){
		return jobsServices.listarPorId(jobId);
	}
	
	@PostMapping("/jobs/create")//Listo(Se debe asignar el id)//Listo
	public ResponseEntity<Object> createJob(@RequestBody JobEntity job){ //requestbody* (recibe un json por esa razon usamos la notacion)
		ResponseEntity<Object> savedJob = jobsServices.create(job);
		return null;
	}
	
	@PutMapping("/jobs/update/{jobId}")
	public void updateJob(@PathVariable String jobsId, @RequestBody JobEntity job) {
		jobsServices.update(jobsId, job);
	}
	
	@DeleteMapping("/jobs/delete/{jobsId}")//Listo
	public void deleteJob(@PathVariable String jobsId) {
		jobsServices.delete(jobsId);
	}
	
	

}
