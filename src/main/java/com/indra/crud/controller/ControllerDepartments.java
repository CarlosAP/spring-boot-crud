package com.indra.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.indra.crud.model.DepartmentEntity;
import com.indra.crud.model.EntityRegion;
import com.indra.crud.services.DepartmentsServices;

@RestController
public class ControllerDepartments {
	
	@Autowired
	private DepartmentsServices services;


	@GetMapping("/departments")///Listo
	public List<DepartmentEntity> getDepartments(){
		return services.listar();
	}
	
	
	@GetMapping("/departments/{departmentId}")//Listo
	public ResponseEntity<Object> getDepartmentById(@PathVariable int departmentId){
		return services.listarPorId(departmentId);
	}
	
	@PostMapping("/departments/create")//Listo(Se debe asignar el id)
	public ResponseEntity<Object> createDepartment(@RequestBody DepartmentEntity department){ //requestbody* (recibe un json por esa razon usamos la notacion)
		ResponseEntity<Object> savedRegion = services.create(department);
		return null;
	}
	
	@PutMapping("/departments/update/{departmentId}")//fk
	public void updateDepartment(@PathVariable int departmentId, @RequestBody DepartmentEntity department) {
		services.update(departmentId, department);
	}
	
	@DeleteMapping("/departments/delete/{departmentId}")//Listo(Funciona para nuevos registros)
	public void deleteDepartment(@PathVariable int departmentId) {
		services.delete(departmentId);
	}
	
	
	
	
	
	
}
