package com.indra.crud.controller;

import java.util.List;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.indra.crud.model.CountriyEntity;
import com.indra.crud.repository.CountriesRepository;
import com.indra.crud.services.CountriesServices;

@Controller
public class CountriesController {

	@Autowired
	private CountriesServices countriesService;
	
	
	@GetMapping("/countries")///L
	public String getCountries(Model model){
			
		model.addAttribute("country", countriesService.getCountries());
		return "countries";
	}
	
	
	@GetMapping("/countries/{countryId}")//L
	public ResponseEntity<Object> getCountry(@PathVariable String countryId){
		return countriesService.getCountry(countryId);
	}
	
	@PostMapping("/countries/create")//L
	public ResponseEntity<Object> createCountry(@RequestBody CountriyEntity country){ //requestbody* (recibe un json por esa razon usamos la notacion)
		ResponseEntity<Object> savedCountry = countriesService.createCountry(country);
		return null;
	}
	
	@PutMapping("/countries/update/{countryID}")
	public void updateCountry(@PathVariable String countryID, @RequestBody CountriyEntity country) {
		countriesService.updateCountry(countryID, country);
	}
	
	@DeleteMapping("/countries/delete/{countryID}")//L
	public void deleteCountry(@PathVariable String countryID) {
		countriesService.deleteCountry(countryID);
	}
	
	

	
}
