package com.indra.crud.services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.indra.crud.model.EmployeeEntity;



public interface EmployeesServices {

	public abstract List<EmployeeEntity> listar();
	public abstract ResponseEntity<Object> listarPorId(int employeeId);
	public abstract ResponseEntity<Object> create(EmployeeEntity employee);
	public abstract void update(int employeeId, EmployeeEntity employee);
	public abstract void delete (int employeeId);
	
	
	
}
