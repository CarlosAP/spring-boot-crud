package com.indra.crud.services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.indra.crud.model.JobEntity;

public interface JobsServices {
	
	
	public abstract List<JobEntity> listar();
	public abstract ResponseEntity<Object> listarPorId(String jobsId);
	public abstract ResponseEntity<Object> create(JobEntity job);
	public abstract void update(String jobsId, JobEntity job);
	public abstract void delete(String jobsId);
	

}
