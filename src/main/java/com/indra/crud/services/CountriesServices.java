package com.indra.crud.services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.indra.crud.model.CountriyEntity;

public interface CountriesServices {
	
	public abstract List<CountriyEntity> getCountries();
	public abstract ResponseEntity<Object> getCountry(String countryID);
	public abstract ResponseEntity<Object> createCountry(CountriyEntity country);
	public abstract void updateCountry(String countryID, CountriyEntity country);
	public abstract void deleteCountry (String countryID);
}
