package com.indra.crud.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.indra.crud.model.CountriyEntity;
import com.indra.crud.repository.CountriesRepository;

@Service
public class CountriesServicesImp implements CountriesServices {

	@Autowired
	private CountriesRepository repoCountry;
	
	public List<CountriyEntity> getCountries(){
		return repoCountry.findAll();
		
	}
	
	public ResponseEntity<Object> getCountry(String countryID){
		return new ResponseEntity<>(repoCountry.findById(countryID), HttpStatus.OK);
	}
	
	public ResponseEntity<Object> createCountry(CountriyEntity country){
		return new ResponseEntity<>(repoCountry.save(country), HttpStatus.OK);
	}
	
	public void updateCountry(String countryID, CountriyEntity country) {
		repoCountry.save(country);
	}
	
	public void deleteCountry(String countryID) {
		repoCountry.deleteById(countryID);
	}
	
}
