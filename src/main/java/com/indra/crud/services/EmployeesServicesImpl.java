package com.indra.crud.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.indra.crud.model.EmployeeEntity;
import com.indra.crud.repository.EmployeesRepository;

@Service
public class EmployeesServicesImpl implements EmployeesServices {

	
	@Autowired 
	private EmployeesRepository employeesRepo;
	
	
	@Override
	public List<EmployeeEntity> listar() {
		return employeesRepo.findAll();
	}

	@Override
	public ResponseEntity<Object> listarPorId(int employeeId) {
		return new ResponseEntity<>(employeesRepo.findById(employeeId), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> create(EmployeeEntity employee) {
		return new ResponseEntity<>(employeesRepo.save(employee), HttpStatus.OK);
	}

	@Override
	public void update(int employeeId, EmployeeEntity employee) {
		employeesRepo.save(employee);
	}

	@Override
	public void delete(int employeeId) {
		employeesRepo.deleteById(employeeId);

	}

}
