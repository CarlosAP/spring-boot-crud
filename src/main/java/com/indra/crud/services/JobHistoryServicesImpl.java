package com.indra.crud.services;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.indra.crud.model.JobHistoryEntity;
import com.indra.crud.model.PKJobHistory;
import com.indra.crud.repository.JobHistoryRepository;

@Service
public class JobHistoryServicesImpl implements JobHistoryServices {

	@Autowired 
	private JobHistoryRepository jobRepo;
	
	
	public List<JobHistoryEntity> listar() {
		return jobRepo.findAll();
	}

	
	public ResponseEntity<Object> listarPorId(PKJobHistory id) {
   
		//return new ResponseEntity<>(jobRepo.findById(new PKJobHistory(102,D)),HttpStatus.OK);
		return new ResponseEntity<>(jobRepo.findById(id),HttpStatus.OK);
	}


	public ResponseEntity<Object> create(JobHistoryEntity employee) {
		return new ResponseEntity<>(jobRepo.save(employee), HttpStatus.OK);
	}


	public void update(PKJobHistory employee, JobHistoryEntity job) {
		jobRepo.save(job);

	}

	
	public void delete(PKJobHistory id) {
		jobRepo.deleteById(id);
	}

}
