package com.indra.crud.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.indra.crud.model.DepartmentEntity;
import com.indra.crud.repository.DepartmentsRepository;

@Service
public class DepartmentsServicesImpl implements DepartmentsServices {

	
	@Autowired //inyecta **
	private DepartmentsRepository repoDepartments;
	
	@Transactional(readOnly=true)
	public List<DepartmentEntity> listar() {
		
		return repoDepartments.findAll();	
	}

		
	@Transactional(readOnly=true)
	public ResponseEntity<Object> listarPorId(int departmentId) {
		return new ResponseEntity<>(repoDepartments.findById(departmentId), HttpStatus.OK);
	}

	
	@Transactional
	public ResponseEntity<Object> create(DepartmentEntity department) {
		return new ResponseEntity<>(repoDepartments.save(department), HttpStatus.OK);
	}

	@Transactional
	public void update(int departmentId, DepartmentEntity department) {
		repoDepartments.save(department);

	}

	@Transactional
	public void delete(int departmentId) {
		repoDepartments.deleteById(departmentId);

	}

}
