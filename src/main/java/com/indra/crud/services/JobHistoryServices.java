package com.indra.crud.services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.indra.crud.model.JobHistoryEntity;
import com.indra.crud.model.PKJobHistory;

public interface JobHistoryServices {
	
	public abstract List<JobHistoryEntity> listar();
	public abstract ResponseEntity<Object> listarPorId(PKJobHistory id);
	public abstract ResponseEntity<Object> create(JobHistoryEntity employee);
	public abstract void update(PKJobHistory employee, JobHistoryEntity job);
	public abstract void delete (PKJobHistory id);

}
