package com.indra.crud.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.indra.crud.model.LocationEntity;
import com.indra.crud.repository.LocationsRepository;

@Service
public class LocationsServicesImpl implements LocationsServices {

	
	@Autowired
	private LocationsRepository locationRepo;
	
	
	
	public List<LocationEntity> listar() {
		return locationRepo.findAll();
	}

	
	public ResponseEntity<Object> listarPorId(int locationId) {
		return new ResponseEntity<>(locationRepo.findById(locationId), HttpStatus.OK);
	}

	
	public ResponseEntity<Object> create(LocationEntity location) {
		return new ResponseEntity<>(locationRepo.save(location), HttpStatus.OK);
	}

	
	public void update(int locationId, LocationEntity location) {
		locationRepo.save(location);
	}

	public void delete(int locationId) {
		locationRepo.deleteById(locationId);

	}

}
