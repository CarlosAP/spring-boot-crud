package com.indra.crud.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.indra.crud.model.JobEntity;
import com.indra.crud.repository.JobsRepository;

@Service
public class JobsServicesImpl implements JobsServices {

	@Autowired
	private JobsRepository jobsRepo;

	@Transactional(readOnly = true)
	public List<JobEntity> listar() {
		return jobsRepo.findAll();
	}

	@Transactional(readOnly = true)
	public ResponseEntity<Object> listarPorId(String jobsId) {

		return new ResponseEntity<>(jobsRepo.findById(jobsId), HttpStatus.OK);
	}

	@Transactional
	public ResponseEntity<Object> create(JobEntity job) {
		 return new ResponseEntity<>(jobsRepo.save(job), HttpStatus.OK);
	}

	@Transactional
	public void update(String jobsId, JobEntity job) {
		jobsRepo.save(job);

	}

	@Transactional
	public void delete(String jobsId) {
		jobsRepo.deleteById(jobsId);

	}

}
