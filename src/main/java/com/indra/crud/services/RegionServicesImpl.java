package com.indra.crud.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.indra.crud.model.EntityRegion;
import com.indra.crud.repository.RegionsRepository;

@Service
public class RegionServicesImpl implements RegionsServices {

	
	@Autowired
	private RegionsRepository repoRegion;
	
	@Transactional(readOnly=true)
	public List<EntityRegion> getRegions(){
		return repoRegion.findAll();
		
	}
	
	@Transactional(readOnly=true)
	public ResponseEntity<Object> getRegionById(int regionId){
		return new ResponseEntity<>(repoRegion.findById(regionId), HttpStatus.OK);
	}
	
	@Transactional
	public ResponseEntity<Object> createRegion(EntityRegion region){
		return new ResponseEntity<>(repoRegion.save(region), HttpStatus.OK);
	}
	
	@Transactional
	public void updateRegion(int regionId, EntityRegion region) {
		repoRegion.save(region);
	}
	
	@Transactional
	public void deleteRegion(int regionId) {
		repoRegion.deleteById(regionId);
	}
	
}
