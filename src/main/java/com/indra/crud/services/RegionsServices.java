package com.indra.crud.services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.indra.crud.model.EntityRegion;



public interface RegionsServices {
	
	public abstract List<EntityRegion> getRegions();
	public abstract ResponseEntity<Object> getRegionById(int regionId);
	public abstract ResponseEntity<Object> createRegion(EntityRegion region);
	public abstract void updateRegion(int regionId, EntityRegion region);
	public abstract void deleteRegion (int regionId);

}
