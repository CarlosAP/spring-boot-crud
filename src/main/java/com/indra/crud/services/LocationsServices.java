package com.indra.crud.services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.indra.crud.model.LocationEntity;



public interface LocationsServices {

	
	public abstract List<LocationEntity> listar();
	public abstract ResponseEntity<Object> listarPorId(int locationId);
	public abstract ResponseEntity<Object> create(LocationEntity location);
	public abstract void update(int locationId, LocationEntity location);
	public abstract void delete(int locationId);
	
	
	
	
	
}
