package com.indra.crud.services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.indra.crud.model.DepartmentEntity;
import com.indra.crud.model.EntityRegion;

public interface DepartmentsServices {


	public abstract List<DepartmentEntity> listar();
	public abstract ResponseEntity<Object> listarPorId(int departmentId);
	public abstract ResponseEntity<Object> create(DepartmentEntity department);
	public abstract void update(int departmentId, DepartmentEntity department);
	public abstract void delete (int departmentId);




}
