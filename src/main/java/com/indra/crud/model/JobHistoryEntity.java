package com.indra.crud.model;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.*;

@Entity
@Table(name="JOB_HISTORY")
//@IdClass(value = JobHistoryEntity.class)
public class JobHistoryEntity implements Serializable{

	/**
	 * Al parecer cuando tenemos una pk compuesta se debe implementar serializable, ademas del private final static
	 * asi como tambien el idcñass que como value llevara el nombre de la clase mas el .class----------------------
	 */
	private static final long serialVersionUID = 1L;

	
	@EmbeddedId
	private PKJobHistory miPK;
	
	
	/*@Id
	@Column(name = "EMPLOYEE_ID")
	private int employeeId;
	
	@Id
	@Column(name = "START_DATE")
	private Date startDate;*/

	@Column(name = "END_DATE")
	private LocalDate endDate;
	
	@Column(name = "JOB_ID")
	private String jobId;
	
	@Column(name = "DEPARTMENT_ID")
	private int departmentId;
	
	
	
	
	
	public JobHistoryEntity(PKJobHistory miPK, LocalDate endDate, String jobId, int departmentId) {
		super();
		this.miPK = miPK;
		this.endDate = endDate;
		this.jobId = jobId;
		this.departmentId = departmentId;
	}



	@Override
	public String toString() {
		return "JobHistoryEntity [miPK=" + miPK + ", endDate=" + endDate + ", jobId=" + jobId + ", departmentId="
				+ departmentId + "]";
	}



	public JobHistoryEntity() {	}



	public PKJobHistory getMiPK() {
		return miPK;
	}



	public void setMiPK(PKJobHistory miPK) {
		this.miPK = miPK;
	}



	public LocalDate getEndDate() {
		return endDate;
	}



	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}



	public String getJobId() {
		return jobId;
	}



	public void setJobId(String jobId) {
		this.jobId = jobId;
	}



	public int getDepartmentId() {
		return departmentId;
	}



	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
	
	
	
	///Get and set Clase sin embedded
	/*public JobHistoryEntity(int employeeId, Date startDate, Date endDate, String jobId, int departmentId) {
		super();
		this.employeeId = employeeId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.jobId = jobId;
		this.departmentId = departmentId;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	@Override
	public String toString() {
		return "JobHistory [employeeId=" + employeeId + ", startDate=" + startDate + ", endDate=" + endDate + ", jobId="
				+ jobId + ", departmentId=" + departmentId + "]";
	}*/
	
	
	
	
	
	
}
