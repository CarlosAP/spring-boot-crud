package com.indra.crud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;

@Entity
@Table(name="DEPARTMENTS")
public class DepartmentEntity {
	
	@Column(name ="DEPARTMENT_ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="DEPARTMENTS_SEQ")
    @SequenceGenerator(name="DEPARTMENTS_SEQ", sequenceName = "DEPARTMENTS_SEQ", allocationSize=1)
	private int departmentId;
	
	@NotNull
	@Column(name = "DEPARTMENT_NAME")
	private String departmentName;
	
	
	@Column(name = "MANAGER_ID")
	
	private Integer managerId;
	
	@Column(name = "LOCATION_ID")
	private int locationId;
	
	
	public DepartmentEntity() {} 
	
	public DepartmentEntity(int departmentId, String departmentName, Integer managerId, int locationId) {
		super();
		this.departmentId = departmentId;
		this.departmentName = departmentName;
		this.managerId = managerId;
		this.locationId = locationId;
	}






	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public Integer getManagerId() {
		return managerId;
	}

	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	@Override
	public String toString() {
		return "DepartamentsEntity [departmentId=" + departmentId + ", departmentName=" + departmentName
				+ ", managerId=" + managerId + ", locationId=" + locationId + "]";
	}
	
	
	
	
	
	
	
	
	
	
}
