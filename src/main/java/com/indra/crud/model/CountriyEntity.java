package com.indra.crud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.indra.crud.model.EntityRegion;



@Entity
@Table(name="Countries")
public class CountriyEntity {
	
	@Id
	@NotNull
	@Column(name="COUNTRY_ID") //Solo puede almacenar 2 caracteres para el id 
	//@GeneratedValue(strategy=GenerationType.IDENTITY) //en caso de que el id sea autoincrementable
	private String countryID;
	
	 
	@Column(name="COUNTRY_NAME") ///******
	private String countryName;
	
	@OneToOne()
	@JoinColumn(name = "REGION_ID")
	private EntityRegion region;
	
	
	/*@Column(name="REGION_ID")
	private int number;
	///Para asignar un id se deben revisar los id ya existentes de la tabla region_id*/
	
	
	public CountriyEntity() {}


	public CountriyEntity(String countryID, String countryName, EntityRegion region) {
		super();
		this.countryID = countryID;
		this.countryName = countryName;
		this.region = region;
	}

	public String getCountryID() {
		return countryID;
	}
	
	public void setCountryID(String countryID) {
		this.countryID = countryID;
	}


	public String getCountryName() {
		return countryName;
	}


	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}


	public EntityRegion getRegion() {
		return region;
	}


	public void setRegion(EntityRegion region) {
		this.region = region;
	}


	@Override
	public String toString() {
		return "CountriesEntity [countryID=" + countryID + ", countryName=" + countryName + ", region=" + region + "]";
	}



	
	
	
	
}
