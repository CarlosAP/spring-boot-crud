package com.indra.crud.model;

import javax.persistence.*;
import com.sun.istack.NotNull;


@Entity
@Table(name="REGIONS")
public class EntityRegion {
	
	@Id
	@NotNull
	@Column(name="REGION_ID")
	//@GeneratedValue(strategy=GenerationType.SEQUENCE) //en caso de que el id sea autoincrementable
	private int regionId;
	
	@Column(name="REGION_NAME")
	private String regionName;
	
	
	
	public EntityRegion() 	{	}
	
	public EntityRegion(int regionId, String regionName) {
		super();
		this.regionId = regionId;
		this.regionName = regionName;
	}

	public int getRegionId() {
		return regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public String getRegionName() {
		return regionName;
	}
	
	@Override
	public String toString() {
		return "EntityRegions [regionId=" + regionId + ", regionName=" + regionName + "]";
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	
	
	
	

}
